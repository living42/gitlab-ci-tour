from flask import Flask, jsonify, request

import calculator


app = Flask(__name__)


@app.route('/add')
def add():
    try:
        x = int(request.args['x'])
        y = int(request.args['y'])
    except KeyError:
        return jsonify({
            'message': 'missing x and y arguments' 
        }), 422
    except ValueError:
        return jsonify({
            'message': 'value error numbers are needed'
        }), 422

    r = calculator.add(x, y)

    return jsonify({'result': r})
