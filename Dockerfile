FROM python:3.7-alpine

ADD requirements.txt /tmp/requirements.txt

RUN pip install -r /tmp/requirements.txt --no-cache --disable-pip-version-check

ADD . /app

WORKDIR /app

EXPOSE 5000

ENV FLASK_APP app.py

CMD ["flask", "run", "-h", "0.0.0.0", "-p", "5000"]
