from calculator import add, multiply, sub


def test_add():
    assert add(1, 1) == 2


def test_multiply():
    assert multiply(1, 1) == 1
    assert multiply(1, 2) == 2


def test_sub():
    assert sub(1, 1) == 0
